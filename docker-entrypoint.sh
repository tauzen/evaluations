#!/bin/bash

if test "$1" == "mock_api"; then
    python mock_api/evaluations_api.py
elif test "$1" == "web_app"; then
    python web_app/app.py
else
    echo "You must provide a command argument when running the container:"
    echo "  - mock_api"
    echo "  - web_app"
    echo "You provided: $@"
    exit 1
fi
