import os

import flask

from mock_api.data import evaluations, executed_runs

app = flask.Flask(__name__)


@app.route("/evaluations/", methods=["GET"])
def get_evaluations():
    user_id = flask.request.args.get("user_id")
    result = evaluations.values()
    if user_id:
        result = filter(lambda e: e["user_id"] == user_id, result)

    return {'evaluations': list(result)}


@app.route("/evaluations/<string:evaluation_id>", methods=["GET"])
def get_evaluation(evaluation_id: str):
    if evaluation_id in evaluations:
        return evaluations[evaluation_id]

    return 404


@app.route("/runs/", methods=["GET"])
def get_runs():
    evaluation_id = flask.request.args.get("evaluation_id")
    if evaluation_id:
        if evaluation_id not in evaluations:
            return 404

        result = executed_runs[evaluation_id]
    else:
        result = list(executed_runs.values())

    return {
        "runs": result
    }


HOST_IP = os.environ.get("HOST_IP", "0.0.0.0")
app.run(host=HOST_IP)
