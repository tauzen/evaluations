from collections import defaultdict
from random import randint
from typing import Dict
import uuid

import arrow


evaluations = {}
executed_runs = defaultdict(list)

AVAILABLE_STATUS = [
    "SCHEDULED",
    "CANCELLED",
    "RUNNING",
    "FINISHED"
]

USERS = [
    "ab0b0e48734811ea8fa77831c1d6f836",
    "b99c122c734811eabaf47831c1d6f836"
]

ROBOTS = [
    "d33f7f20734811ea86ab7831c1d6f836",
    "de03ffda734811ea97327831c1d6f836"
]


def create_evaluation(
    user_id: str,
    robot_id: str
) -> Dict:
    evaluation_id = uuid.uuid4().hex

    branch = "branch_" + str(randint(1000, 9999))
    runs = randint(1, 10)

    status = AVAILABLE_STATUS[randint(0, 3)]

    date = arrow.utcnow().shift(hours=-randint(1, 100))
    created_at = str(date)
    date.shift(hours=+randint(1, 10))
    updated_at = str(date)

    return {
        "evaluation_id": evaluation_id,
        "user_id": user_id,
        "robot_id": robot_id,
        "branch": branch,
        "runs": runs,
        "status": status,
        "created_at": created_at,
        "updated_at": updated_at
    }


def create_run(
    evaluation_id: str,
    operator_id: str,
    start_time: str,
) -> Dict:
    run_id = uuid.uuid4().hex
    all_attempts = randint(1, 10)
    successful_attempts = randint(1, all_attempts)

    return {
        "run_id": run_id,
        "evaluation_id": evaluation_id,
        "operator_id": operator_id,
        "all_attempts": all_attempts,
        "successful_attempts": successful_attempts,
        "start_time": start_time,
        "end_time": start_time,
        "created_at": start_time
    }


def generate_mock_data():
    for i in range(1, 10):
        user_id = USERS[randint(0, len(USERS) - 1)]
        robot_id = ROBOTS[randint(0, len(ROBOTS) - 1)]
        e = create_evaluation(user_id, robot_id)
        evaluations[e["evaluation_id"]] = e

        runs = 0
        if e["status"] == "RUNNING":
            runs = randint(0, e["runs"])
        elif e["status"] == "FINISHED":
            runs = e["runs"]

        for _ in range(runs):
            executed_runs[e["evaluation_id"]].append(
                create_run(
                    e["evaluation_id"],
                    e["user_id"],
                    e["created_at"]
                )
            )


generate_mock_data()
