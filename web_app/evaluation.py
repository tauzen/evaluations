from dataclasses import dataclass
from enum import Enum
from typing import List, Optional
from uuid import UUID

from arrow import Arrow


class EvaluationStatus(Enum):
    SCHEDULED = "SCHEDULED"
    CANCELLED = "CANCELLED"
    RUNNING = "RUNNING"
    FINISHED = "FINISHED"


@dataclass
class Run:
    id: UUID
    evaluation_id: UUID
    operator_id: UUID
    all_attempts: int
    successful_attempts: int
    start_time: Arrow
    end_time: Arrow
    created_at: Arrow

    @property
    def summary(self) -> str:
        return f"{self.successful_attempts} / {self.all_attempts}"


@dataclass
class Evaluation:
    id: UUID
    user_id: UUID
    robot_id: Optional[UUID]
    branch: str
    status: EvaluationStatus
    runs: int
    created_at: Arrow
    updated_at: Arrow

    executed_runs: List[Run]

    @property
    def progress(self) -> str:
        if (
            self.status == EvaluationStatus.SCHEDULED or
            self.status == EvaluationStatus.CANCELLED
        ):
            return "-"

        return f"{len(self.executed_runs)} / {self.runs}"
