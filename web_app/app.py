import flask

from web_app import settings
from web_app.api_client import APIClient


app = flask.Flask(__name__)


@app.route("/", methods=["GET"])
def list_evaluations():
    api = APIClient(settings.EVALUATION_API_BASE_URL)
    evaluations = api.get_evaluations()

    return flask.render_template(
        settings.INDEX_TEMPLATE,
        evaluations=evaluations
    )


@app.route("/evaluation/<string:evaluation_id>", methods=["GET"])
def evaluation_details(evaluation_id: str):
    api = APIClient(settings.EVALUATION_API_BASE_URL)
    evaluation = api.get_evaluation(evaluation_id)

    return flask.render_template(
        settings.DETAILS_TEMPLATE,
        evaluation=evaluation
    )


app.run(host=settings.HOST_IP)