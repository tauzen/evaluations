from typing import List, Dict
from uuid import UUID

import arrow
import requests

from web_app.evaluation import Evaluation, EvaluationStatus, Run


class APIException(Exception):
    pass


class APIClient:
    _EVALUATION_RESOURCE: str = "/evaluations"
    _RUN_RESOURCE: str = "/runs"

    def __init__(self, base_url: str):
        self._evaluations_uri = base_url + self._EVALUATION_RESOURCE
        self._runs_uri = base_url + self._RUN_RESOURCE

    def get_evaluations(self) -> List[Evaluation]:
        result = requests.get(self._evaluations_uri)
        if result.status_code != 200:
            raise APIException

        return [self._to_evaluation(e) for e in result.json()["evaluations"]]

    def get_evaluation(self, evaluation_id: str) -> Evaluation:
        uri = f"{self._evaluations_uri}/{evaluation_id}"
        result = requests.get(uri)
        if result.status_code != 200:
            raise APIException

        evaluation = self._to_evaluation(result.json())
        for run in self.get_runs(evaluation_id):
            evaluation.executed_runs.append(run)

        return evaluation

    def get_runs(self, evaluation_id: str) -> List[Run]:
        uri = f"{self._runs_uri}/?evaluation_id={evaluation_id}"
        result = requests.get(uri)
        if result.status_code != 200:
            raise APIException

        return [self._to_run(r) for r in result.json()["runs"]]

    @staticmethod
    def _to_evaluation(eval_dict: Dict) -> Evaluation:
        evaluation = Evaluation(
            id=UUID(eval_dict["evaluation_id"]),
            user_id=UUID(eval_dict["user_id"]),
            robot_id=UUID(eval_dict["robot_id"]),
            branch=eval_dict["branch"],
            runs=int(eval_dict["runs"]),
            status=EvaluationStatus(eval_dict["status"]),
            created_at=arrow.get(eval_dict["created_at"]),
            updated_at=arrow.get(eval_dict["updated_at"]),
            executed_runs=[]
        )

        return evaluation

    @staticmethod
    def _to_run(run_dict: Dict) -> Run:
        run = Run(
            id=UUID(run_dict["run_id"]),
            evaluation_id=UUID(run_dict["evaluation_id"]),
            operator_id=UUID(run_dict["operator_id"]),
            all_attempts=int(run_dict["all_attempts"]),
            successful_attempts=int(run_dict["successful_attempts"]),
            start_time=arrow.get(run_dict["start_time"]),
            end_time=arrow.get(run_dict["end_time"]),
            created_at=arrow.get(run_dict["created_at"])
        )

        return run
