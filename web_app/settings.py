import os


EVALUATION_API_BASE_URL = os.environ.get(
    "EVALUATION_API_BASE_URL", "http://127.0.0.1:5001"
)

INDEX_TEMPLATE = os.environ.get("INDEX_TEMPLATE", "index.html")
DETAILS_TEMPLATE = os.environ.get("DETAILS_TEMPLATE", "details.html")

HOST_IP = os.environ.get("HOST_IP", "0.0.0.0")