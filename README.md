## Intro
Design doc: [click](https://docs.google.com/document/d/1ybshhX4fp37BQUnFXigyYwp0t10UlZ7eUb41MStj9wA/edit?usp=sharing) 

This is the simplified version of Web Server (web_app) using Evaluations Service API (mock_api).

## Runnig
```
$ docker-compose build
$ docker-compose up
```
Confirm it's runnig by:
```
$ curl http://127.0.0.1:5000/
```
## What is missing
* proper web server for web_app - flask dev server is used,
gunicorn would be a good choice here. 
* tests - unit, integration
* error handling
* traffic and storage estimations (in Design doc)