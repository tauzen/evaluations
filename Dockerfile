FROM python:3.8

RUN pip install --upgrade pip

RUN set -x && addgroup --system app && adduser --system --group app

COPY ./requirements.txt requirements.txt
RUN pip install -r requirements.txt

COPY . /app
USER app
WORKDIR /app
ENV PYTHONPATH /app

ENTRYPOINT ["/app/docker-entrypoint.sh"]
